package com.evanemran.gitcrawler.app.home.views

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.evanemran.gitcrawler.R
import com.evanemran.gitcrawler.app.home.models.FeedItem
import com.evanemran.gitcrawler.utils.AppDateFormatter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import java.text.SimpleDateFormat
import java.util.*

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val selectedFeed: FeedItem = intent.getSerializableExtra("content") as FeedItem

        populateViews(selectedFeed)

    }

    private fun populateViews(selectedFeed: FeedItem) {
        textView_desc_title.text = selectedFeed.fullName
        textView_desc_desc.text = selectedFeed.description

        Picasso.get().load(selectedFeed.owner?.avatarUrl).into(imageView_desc)

        textView_owner.text = "Owner: ${selectedFeed.owner!!.login}"
        textView_stars.text = "Stars Count: ${selectedFeed.stargazersCount}"
        textView_updated.text = "Last Updated: ${AppDateFormatter().formatUpdatedDate(selectedFeed.updatedAt)}"
    }
}