package com.evanemran.gitcrawler.app.home.views

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.evanemran.gitcrawler.R
import com.evanemran.gitcrawler.app.home.adapters.FeedListAdapter
import com.evanemran.gitcrawler.app.home.models.FeedItem
import com.evanemran.gitcrawler.app.home.models.FeedResponse
import com.evanemran.gitcrawler.manager.`interface`.ClickListener
import com.evanemran.gitcrawler.manager.`interface`.ResponseListener
import com.evanemran.gitcrawler.manager.controller.RequestManager
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    lateinit var dialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        dialog = ProgressDialog(this)
        dialog.setTitle("Loading")

        val requestManager: RequestManager = RequestManager(this)

        requestManager.getFeed(headlineResponseListener, "Android", "stars")
        dialog.show()
    }

    private val headlineResponseListener: ResponseListener<FeedResponse> = object : ResponseListener<FeedResponse> {
        override fun didFetch(message: String, response: FeedResponse) {
            dialog.dismiss()
            recyclerView_home.setHasFixedSize(true)
            recyclerView_home.layoutManager = LinearLayoutManager(this@HomeActivity, LinearLayoutManager.VERTICAL, false)
            val adapter: FeedListAdapter = FeedListAdapter(this@HomeActivity, response.items, clickListener)
            recyclerView_home.adapter = adapter

        }

        override fun didError(message: String) {
            dialog.dismiss()
            Toast.makeText(this@HomeActivity, message, Toast.LENGTH_LONG).show()
        }

    }

    private val clickListener: ClickListener<FeedItem> = object : ClickListener<FeedItem> {
        override fun onClicked(item: FeedItem) {
            startActivity(
                Intent(this@HomeActivity, DetailsActivity::class.java)
                .putExtra("content", item))
        }

    }
}