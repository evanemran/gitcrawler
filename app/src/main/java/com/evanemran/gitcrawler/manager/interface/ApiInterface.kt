package com.evanemran.gitcrawler.manager.`interface`

import com.evanemran.gitcrawler.app.home.models.FeedResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiInterface {

    @GET("search/repositories")
    fun search(
        @Query("q") keyword: String,
        @Query("sort") sort: String,
    ): Call<FeedResponse>

}