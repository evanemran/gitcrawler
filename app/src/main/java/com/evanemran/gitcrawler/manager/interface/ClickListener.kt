package com.evanemran.gitcrawler.manager.`interface`

interface ClickListener<T> {
    fun onClicked(item: T)
}