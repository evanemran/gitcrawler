package com.evanemran.gitcrawler.manager.`interface`

interface ResponseListener<T> {
    fun didFetch(message: String, response: T)
    fun didError(message: String)
}